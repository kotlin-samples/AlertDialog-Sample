package com.mathi.alertdialogsample

import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    internal var builder: AlertDialog.Builder? = null

    internal var alert1: Button? = null
    internal var alert2: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        alert1 = findViewById(R.id.alert1) as Button
        alert2 = findViewById(R.id.alert2) as Button

        alert1!!.setOnClickListener(alert1OnClickListener)
        alert2!!.setOnClickListener(alert2OnClickListener)

        builder = AlertDialog.Builder(this@MainActivity)
    }

    private val alert1OnClickListener = View.OnClickListener { showAlert1() }

    private val alert2OnClickListener = View.OnClickListener { showAlert2() }

    private fun showAlert1() {
        builder!!.setTitle(getText(R.string.alert_1))
        builder!!.setMessage(getText(R.string.alert_1_msg))
        builder!!.setIcon(R.drawable.ic_done)
        builder!!.setPositiveButton(getText(R.string.ok), okOnClickListener)
        builder!!.show()
    }

    private val okOnClickListener = DialogInterface.OnClickListener { dialog, which -> Toast.makeText(this@MainActivity, getText(R.string.you_pressed_ok), Toast.LENGTH_SHORT).show() }

    private fun showAlert2() {
        builder!!.setTitle(getText(R.string.alert_2))
        builder!!.setMessage(getText(R.string.alert_2_msg))
        builder!!.setIcon(R.drawable.ic_done)
        builder!!.setPositiveButton(getText(R.string.ok), okOnClickListener)
        builder!!.setNegativeButton(getText(R.string.cancel), cancelOnClickListener)
        builder!!.setNeutralButton(getText(R.string.close), closeOnClickListener)
        builder!!.show()
    }

    private val cancelOnClickListener = DialogInterface.OnClickListener { dialog, which -> Toast.makeText(this@MainActivity, getText(R.string.you_pressed_cancel), Toast.LENGTH_SHORT).show() }

    private val closeOnClickListener = DialogInterface.OnClickListener { dialog, which -> Toast.makeText(this@MainActivity, getText(R.string.you_pressed_close), Toast.LENGTH_SHORT).show() }
}
